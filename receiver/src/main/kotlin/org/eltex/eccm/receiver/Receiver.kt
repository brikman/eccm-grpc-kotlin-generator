package org.eltex.eccm.receiver

import io.grpc.Status
import io.grpc.StatusException
import kotlinx.coroutines.flow.Flow
import org.eltex.eccm.grpc.TestServiceGrpcBase
import org.eltex.eccm.grpc.context.TraceId
import org.eltex.eccm.grpc.BidiStreamRequest
import org.eltex.eccm.grpc.BidiStreamResponse
import org.eltex.eccm.grpc.ClientStreamRequest
import org.eltex.eccm.grpc.ClientStreamResponse
import org.eltex.eccm.grpc.SendRequest
import org.eltex.eccm.grpc.SendResponse
import org.eltex.eccm.grpc.ServerStreamRequest
import org.eltex.eccm.grpc.ServerStreamResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import kotlin.coroutines.coroutineContext

@Component
class Receiver : TestServiceGrpcBase() {

    override suspend fun send(request: SendRequest): SendResponse {
        logger.info("[${coroutineContext[TraceId]}] Message received: ${request.text}; metadata: ${metadata()}")

        // throw AnswerContract(500, "AnswerContract error", "Test error reason").toStatusRuntimeException()
        // throw AnswerContractException(500, "AnswerContractException error", "Test error reason")
        // throw Status.FAILED_PRECONDITION.asException()

        return SendResponse()
    }

    override suspend fun clientStream(requests: Flow<ClientStreamRequest>): ClientStreamResponse {
        requests.collect { logger.info("Stream received: ${it.text}") }
        return ClientStreamResponse()
    }

    override fun serverStream(request: ServerStreamRequest): Flow<ServerStreamResponse> {
        throw StatusException(Status.UNIMPLEMENTED)
    }

    override fun bidiStream(requests: Flow<BidiStreamRequest>): Flow<BidiStreamResponse> {
        throw StatusException(Status.UNIMPLEMENTED)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.declaringClass)
    }
}