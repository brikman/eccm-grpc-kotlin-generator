package io.grpc.internal

import com.google.common.base.Stopwatch
import io.grpc.NameResolver
import io.grpc.NameResolverProvider
import org.eltex.eccm.grpc.client.GrpcClientProperties
import org.slf4j.LoggerFactory
import java.net.URI
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.annotation.PreDestroy
import kotlin.time.toKotlinDuration

class RediscoveringDnsNameResolverProvider(
    grpcClientProperties: GrpcClientProperties
) : NameResolverProvider() {
    private val refreshScheduler by lazy { Executors.newScheduledThreadPool(1) }
    private val refreshSchedulerStarted = AtomicBoolean(false)
    private val resolvers = ConcurrentHashMap.newKeySet<NameResolver>()

    private val rediscoverInterval = grpcClientProperties.rediscoverInterval.toKotlinDuration()

    init {
        require(rediscoverInterval.inWholeSeconds > 5) { "Rediscover interval must not be lesser than 5 seconds" }
        logger.info("Rediscovering DNS name resolver provider registered with rediscover interval: $rediscoverInterval")
    }

    /**
     * Copy of [io.grpc.internal.DnsNameResolverProvider]
     */
    override fun newNameResolver(targetUri: URI, args: NameResolver.Args): NameResolver? {
        if (targetUri.scheme != SCHEME) return null
        val targetPath = requireNotNull(targetUri.path) { "URI has empty path" }
        require(targetPath.startsWith("/")) { "Path ($targetPath) of the URI ($targetUri) must start with '/'" }
        val resolver = RetryingNameResolver(
            DnsNameResolverDelegate(
                targetUri.authority,
                targetPath.substring(1),
                args
            ),
            BackoffPolicyRetryScheduler(
                ExponentialBackoffPolicy.Provider(),
                args.scheduledExecutorService,
                args.synchronizationContext
            ),
            args.synchronizationContext
        )
        resolvers += resolver
        scheduleAutoRefresh()
        return resolver
    }

    override fun getDefaultScheme(): String {
        return SCHEME
    }

    override fun isAvailable(): Boolean {
        return true
    }

    /**
     * More important than [DnsNameResolverProvider.priority].
     */
    override fun priority(): Int {
        return 6
    }

    @PreDestroy
    fun destroy() {
        refreshScheduler.shutdownNow()
        resolvers.clear()
    }

    private fun refresh() {
        resolvers.forEach { it.refresh() }
    }

    private fun scheduleAutoRefresh() {
        if (refreshSchedulerStarted.compareAndSet(false, true)) {
            refreshScheduler.scheduleAtFixedRate(
                ::refresh,
                rediscoverInterval.inWholeSeconds,
                rediscoverInterval.inWholeSeconds,
                TimeUnit.SECONDS
            )
        }
    }

    private inner class DnsNameResolverDelegate(
        authority: String?,
        name: String,
        args: Args
    ) : DnsNameResolver(
        authority,
        name,
        args,
        GrpcUtil.SHARED_CHANNEL_EXECUTOR,
        Stopwatch.createUnstarted(),
        false
    ) {
        override fun shutdown() {
            resolvers.remove(this)
            super.shutdown()
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.declaringClass)

        private const val SCHEME = "dns"
    }
}