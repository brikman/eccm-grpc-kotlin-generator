package org.eltex.eccm.grpc.server

import org.springframework.context.annotation.ConditionContext
import org.springframework.context.annotation.ConfigurationCondition
import org.springframework.context.annotation.ConfigurationCondition.ConfigurationPhase
import org.springframework.core.type.AnnotatedTypeMetadata

class GrpcServicesPresentCondition : ConfigurationCondition {

    override fun getConfigurationPhase(): ConfigurationPhase {
        return ConfigurationPhase.REGISTER_BEAN
    }

    override fun matches(context: ConditionContext, metadata: AnnotatedTypeMetadata): Boolean {
        return requireNotNull(context.beanFactory) { "ConfigurableListableBeanFactory is not available" }
            .getBeanNamesForType(AbstractGrpcService::class.java).isNotEmpty()
    }
}