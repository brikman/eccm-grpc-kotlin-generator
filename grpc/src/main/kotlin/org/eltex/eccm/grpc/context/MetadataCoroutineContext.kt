package org.eltex.eccm.grpc.context

import io.grpc.Metadata
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

class MetadataCoroutineContext(
    val metadata: Metadata
) : AbstractCoroutineContextElement(MetadataCoroutineContext) {

    companion object Key : CoroutineContext.Key<MetadataCoroutineContext> {
        suspend fun getMetadata(): Metadata {
            return coroutineContext[this]?.metadata
                ?: throw IllegalStateException("No io.grpc.Metadata object found in current coroutine context")
        }
    }
}
