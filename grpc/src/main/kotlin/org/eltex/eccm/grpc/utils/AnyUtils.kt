package org.eltex.eccm.grpc.utils

import com.google.protobuf.ByteString
import pbandk.encodeToByteArray
import pbandk.pack

private const val DEFAULT_TYPE_URL_PREFIX = "type.googleapis.com"

fun pbandk.Message.packToAny(): pbandk.wkt.Any =
    pbandk.wkt.Any.pack(this)

fun pbandk.Message.packToGrpcAny(): com.google.protobuf.Any =
    com.google.protobuf.Any.newBuilder()
        .setValue(ByteString.copyFrom(this.encodeToByteArray()))
        .setTypeUrl("$DEFAULT_TYPE_URL_PREFIX/${this.descriptor.fullName}")
        .build()

fun pbandk.wkt.Any.toGrpc(): com.google.protobuf.Any =
    com.google.protobuf.Any.newBuilder()
        .setValue(ByteString.copyFrom(this.value.array))
        .setTypeUrl(this.typeUrl)
        .build()

fun com.google.protobuf.Any.toPbandk(): pbandk.wkt.Any =
    pbandk.wkt.Any(
        typeUrl = this.typeUrl,
        value = pbandk.ByteArr(this.value.toByteArray()),
    )
