package org.eltex.eccm.grpc.server

import org.hibernate.validator.constraints.Range
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.validation.annotation.Validated

@Validated
@ConfigurationProperties(prefix = "grpc.server")
data class GrpcServerProperties(

    val enabled: Boolean = false,

    @field:Range(min = 1, max = 65535)
    val port: Int = DEFAULT_PORT,
) {

    companion object {
        const val DEFAULT_PORT = 8888
    }
}
