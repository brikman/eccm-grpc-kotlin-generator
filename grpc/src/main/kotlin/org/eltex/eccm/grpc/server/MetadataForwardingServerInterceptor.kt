package org.eltex.eccm.grpc.server

import io.grpc.Metadata
import io.grpc.ServerCall
import io.grpc.kotlin.CoroutineContextServerInterceptor
import org.eltex.eccm.grpc.context.MetadataForwarders
import org.eltex.eccm.grpc.context.MetadataCoroutineContext
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@Order(Ordered.HIGHEST_PRECEDENCE)
class MetadataForwardingServerInterceptor(
    private val metadataForwarders: MetadataForwarders,
) : CoroutineContextServerInterceptor() {

    override fun coroutineContext(call: ServerCall<*, *>, headers: Metadata): CoroutineContext {
        var resultCoroutineContext = EmptyCoroutineContext + MetadataCoroutineContext(headers)
        metadataForwarders.restoreFromHeaders(headers).forEach { resultCoroutineContext += it }
        return resultCoroutineContext
    }
}