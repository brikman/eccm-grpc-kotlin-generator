package org.eltex.eccm.grpc.context

import io.grpc.Metadata
import kotlin.coroutines.CoroutineContext

class MetadataForwarders(
    private val forwarders: List<MetadataForwarder<*>>
) {

    internal suspend fun fillHeaders(headers: Metadata) {
        forwarders.forEach { it.set(headers) }
    }

    internal fun restoreFromHeaders(headers: Metadata): List<CoroutineContext.Element> {
        return forwarders.mapNotNull { it.get(headers) }
    }
}