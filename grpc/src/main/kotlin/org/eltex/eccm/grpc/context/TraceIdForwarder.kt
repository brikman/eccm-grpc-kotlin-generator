package org.eltex.eccm.grpc.context

import java.util.UUID

class TraceIdForwarder : MetadataForwarder<TraceId>(TraceId.Key) {

    override suspend fun extractValue(element: TraceId): String {
        return element.uuid.toString()
    }

    override fun buildFromValue(value: String): TraceId {
        return TraceId(UUID.fromString(value))
    }
}