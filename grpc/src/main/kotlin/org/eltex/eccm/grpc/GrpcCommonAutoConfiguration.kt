package org.eltex.eccm.grpc

import org.eltex.eccm.grpc.context.MetadataForwarder
import org.eltex.eccm.grpc.context.MetadataForwarders
import org.eltex.eccm.grpc.context.TraceIdForwarder
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Bean

@AutoConfiguration
class GrpcCommonAutoConfiguration {

    @Bean
    fun traceIdForwarder(): TraceIdForwarder {
        return TraceIdForwarder()
    }

    @Bean
    fun metadataForwarders(
        forwarders: List<MetadataForwarder<*>>
    ): MetadataForwarders {
        return MetadataForwarders(forwarders)
    }
}