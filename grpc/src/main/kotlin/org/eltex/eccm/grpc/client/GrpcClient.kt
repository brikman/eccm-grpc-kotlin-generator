package org.eltex.eccm.grpc.client

import io.grpc.CallOptions
import io.grpc.Channel
import io.grpc.Metadata
import io.grpc.MethodDescriptor
import io.grpc.kotlin.ClientCalls
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import org.eltex.eccm.grpc.AnswerContractException
import org.eltex.eccm.grpc.context.MetadataForwarders
import org.eltex.eccm.grpc.server.GrpcServerProperties
import org.eltex.eccm.grpc.utils.toAnswerContract
import pbandk.Message
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import kotlin.time.Duration
import kotlin.time.toKotlinDuration

class GrpcClient(
    private val grpcClientProperties: GrpcClientProperties,
    private val metadataForwarders: MetadataForwarders,
) {
    private val channels = ConcurrentHashMap<GrpcTarget, Channel>()
    private val defaultCallOptions = buildDefaultCallOptions()

    private val defaultTimeout = grpcClientProperties.defaultTimeout.toKotlinDuration()
    private val defaultRetries = grpcClientProperties.defaultRetries

    suspend fun <R : Message, S : Message> unaryRpc(
        request: R,
        method: MethodDescriptor<R, S>,
        target: GrpcTarget,
        timeout: Duration? = null,
        retries: Int? = null,
    ): S {
        return withCallSpec(target, timeout, retries) { callSpec ->
            ClientCalls.unaryRpc(
                channel = callSpec.channel,
                method = method,
                request = request,
                callOptions = callSpec.callOptions,
                headers = callSpec.headers,
            )
        }
    }

    suspend fun <R : Message, S : Message> clientStreamingRpc(
        requests: Flow<R>,
        method: MethodDescriptor<R, S>,
        target: GrpcTarget,
        timeout: Duration? = null,
        retries: Int? = null,
    ): S {
        return withCallSpec(target, timeout, retries) { callSpec ->
            ClientCalls.clientStreamingRpc(
                channel = callSpec.channel,
                method = method,
                requests = requests,
                callOptions = callSpec.callOptions,
                headers = callSpec.headers,
            )
        }
    }

    suspend fun <R : Message, S : Message> serverStreamingRpc(
        request: R,
        method: MethodDescriptor<R, S>,
        target: GrpcTarget,
        timeout: Duration? = null,
        retries: Int? = null,
    ): Flow<S> {
        return withCallSpec(target, timeout, retries) { callSpec ->
            ClientCalls.serverStreamingRpc(
                channel = callSpec.channel,
                method = method,
                request = request,
                callOptions = callSpec.callOptions,
                headers = callSpec.headers,
            ).catch { wrapAndRethrow(it) }
        }
    }

    suspend fun <R : Message, S : Message> bidiStreamingRpc(
        requests: Flow<R>,
        method: MethodDescriptor<R, S>,
        target: GrpcTarget,
        timeout: Duration? = null,
        retries: Int? = null,
    ): Flow<S> {
        return withCallSpec(target, timeout, retries) { callSpec ->
            ClientCalls.bidiStreamingRpc(
                channel = callSpec.channel,
                method = method,
                requests = requests,
                callOptions = callSpec.callOptions,
                headers = callSpec.headers,
            ).catch { wrapAndRethrow(it) }
        }
    }

    private suspend fun prepareCallSpec(target: GrpcTarget, timeout: Duration?, retries: Int?): CallSpec {
        require(timeout == null || timeout.isPositive()) { "Timeout must be positive" }
        require(retries == null || retries > 0) { "Retries must be positive" }

        val channel = channels.computeIfAbsent(target) { buildChannel(it) }

        val headers = Metadata()
        metadataForwarders.fillHeaders(headers)

        var callOptions = defaultCallOptions

        val callTimeout = timeout?.takeIf { it.isPositive() } ?: defaultTimeout
        callOptions = callOptions.withDeadlineAfter(callTimeout)

        return CallSpec(
            channel = channel,
            callOptions = callOptions,
            headers = headers,
            timeout = callTimeout,
            retries = retries ?: defaultRetries
        )
    }

    private fun buildChannel(target: GrpcTarget): Channel {
        val defaultTargetAddress = "dns:/${target.hostname}:${GrpcServerProperties.DEFAULT_PORT}"
        val channelPropertiesOverrides = grpcClientProperties.channels[target.serviceName]
        val targetAddress = channelPropertiesOverrides?.address?.toString() ?: defaultTargetAddress

        return NettyChannelBuilder.forTarget(targetAddress)
            .usePlaintext()
            .defaultLoadBalancingPolicy(GrpcClientProperties.ROUND_ROBIN_LOAD_BALANCING_STRATEGY)

            // TODO AbstractChannelFactory.configure()

            .build()
    }

    private fun buildDefaultCallOptions(): CallOptions {
        return CallOptions.DEFAULT
    }

    private suspend inline fun <T> withCallSpec(
        target: GrpcTarget,
        timeout: Duration?,
        retries: Int?,
        call: (CallSpec) -> T
    ): T {
        return prepareCallSpec(target, timeout, retries).let { callSpec ->
            runCatching {
                retry(callSpec, call)
            }.getOrElse {
                wrapAndRethrow(it)
            }
        }
    }

    private suspend inline fun <T> retry(callSpec: CallSpec, call: (CallSpec) -> T): T {
        if (callSpec.retries == 0) return call(callSpec)

        var nextCallAt = 0L
        var e: Throwable? = null
        for (attempt in 1..callSpec.retries + 1) {
            runCatching {
                nextCallAt = System.currentTimeMillis() + callSpec.timeout.inWholeMilliseconds

                val callSpecWithFixedDeadline = if (attempt > 1) {
                    callSpec.copy(callOptions = callSpec.callOptions.withDeadlineAfter(callSpec.timeout))
                } else callSpec

                return call(callSpecWithFixedDeadline)
            }.onFailure {
                e = it
                @Suppress("KotlinConstantConditions") // false-positive
                delay(nextCallAt - System.currentTimeMillis())
            }
        }

        // retries exhausted
        wrapAndRethrow(e!!)
    }

    private fun wrapAndRethrow(e: Throwable): Nothing {
        throw if (e is AnswerContractException) throw e else AnswerContractException(e.toAnswerContract())
    }

    private fun CallOptions.withDeadlineAfter(duration: Duration): CallOptions {
        return withDeadlineAfter(duration.inWholeMilliseconds, TimeUnit.MILLISECONDS)
    }

    private data class CallSpec(
        val channel: Channel,
        val callOptions: CallOptions,
        val headers: Metadata,
        val timeout: Duration,
        val retries: Int,
    )
}