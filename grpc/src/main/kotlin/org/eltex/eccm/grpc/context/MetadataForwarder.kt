package org.eltex.eccm.grpc.context

import io.grpc.Metadata
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

abstract class MetadataForwarder<E : CoroutineContext.Element>(
    private val coroutineContextKey: CoroutineContext.Key<E>,
) {
    private val metadataKey = Metadata.Key.of(
        coroutineContextKey::class.qualifiedName,
        Metadata.ASCII_STRING_MARSHALLER
    )

    abstract suspend fun extractValue(element: E): String

    abstract fun buildFromValue(value: String): E

    suspend fun set(metadata: Metadata) {
        coroutineContext[coroutineContextKey]?.also { e ->
            metadata.put(metadataKey, extractValue(e))
        }
    }

    fun get(metadata: Metadata): E? {
        return metadata.get(metadataKey)?.let { buildFromValue(it) }
    }
}
