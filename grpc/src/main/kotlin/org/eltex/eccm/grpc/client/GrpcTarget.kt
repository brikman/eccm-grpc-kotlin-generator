package org.eltex.eccm.grpc.client

data class GrpcTarget(
    val serviceName: String,
    val hostname: String,
)
