package org.eltex.eccm.grpc.server

import io.grpc.ServerInterceptor
import org.eltex.eccm.grpc.GrpcCommonAutoConfiguration
import org.eltex.eccm.grpc.context.MetadataForwarders
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Conditional

@AutoConfiguration(after = [GrpcCommonAutoConfiguration::class])
@EnableConfigurationProperties(GrpcServerProperties::class)
@Conditional(GrpcServicesPresentCondition::class)
class GrpcServerAutoConfiguration {

    @Bean
    @ConditionalOnProperty(name = ["grpc.server.enabled"], havingValue = "true", matchIfMissing = false)
    fun grpcServerFactory(
        properties: GrpcServerProperties,
        serviceBeans: List<AbstractGrpcService>,
        globalInterceptors: List<ServerInterceptor>,
    ): GrpcServerFactory {
        return GrpcServerFactory(
            properties = properties,
            serviceBeans = serviceBeans,
            globalInterceptors = globalInterceptors,
        )
    }

    @Bean
    @ConditionalOnBean(GrpcServerFactory::class)
    fun grpcServerRunner(
        grpcServerFactory: GrpcServerFactory
    ): GrpcServerRunner {
        return GrpcServerRunner(grpcServerFactory)
    }

    @Bean
    @ConditionalOnBean(GrpcServerRunner::class)
    fun metadataPropagatingServerInterceptor(
        metadataForwarders: MetadataForwarders
    ): MetadataForwardingServerInterceptor {
        return MetadataForwardingServerInterceptor(metadataForwarders)
    }
}