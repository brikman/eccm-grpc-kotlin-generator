package org.eltex.eccm.grpc.server

import io.grpc.BindableService
import io.grpc.MethodDescriptor
import io.grpc.ServerMethodDefinition
import io.grpc.kotlin.ServerCalls
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import org.eltex.eccm.grpc.AnswerContractException
import org.eltex.eccm.grpc.utils.toAnswerContract
import org.eltex.eccm.grpc.utils.toStatusRuntimeException
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class AbstractGrpcService(
    private val context: CoroutineContext = EmptyCoroutineContext
) : BindableService {

    protected fun <R, S> unaryServerMethodDefinition(
        descriptor: MethodDescriptor<R, S>,
        implementation: suspend (request: R) -> S
    ): ServerMethodDefinition<R, S> =
        ServerCalls.unaryServerMethodDefinition(
            context = context,
            descriptor = descriptor,
            implementation = wrapSingleResponseCall(implementation),
        )

    protected fun <R, S> clientStreamingServerMethodDefinition(
        descriptor: MethodDescriptor<R, S>,
        implementation: suspend (requests: Flow<R>) -> S
    ): ServerMethodDefinition<R, S> =
        ServerCalls.clientStreamingServerMethodDefinition(
            context = context,
            descriptor = descriptor,
            implementation = wrapSingleResponseCall(implementation),
        )

    protected fun <R, S> serverStreamingServerMethodDefinition(
        descriptor: MethodDescriptor<R, S>,
        implementation: (request: R) -> Flow<S>
    ): ServerMethodDefinition<R, S> =
        ServerCalls.serverStreamingServerMethodDefinition(
            context = context,
            descriptor = descriptor,
            implementation = wrapFlowResponseCall(implementation),
        )

    protected fun <R, S> bidiStreamingServerMethodDefinition(
        descriptor: MethodDescriptor<R, S>,
        implementation: (requests: Flow<R>) -> Flow<S>
    ): ServerMethodDefinition<R, S> =
        ServerCalls.bidiStreamingServerMethodDefinition(
            context = context,
            descriptor = descriptor,
            implementation = wrapFlowResponseCall(implementation),
        )

    private fun <R, S> wrapSingleResponseCall(implementation: suspend (R) -> S): suspend (R) -> S =
        { runCatching { implementation(it) }.getOrElse { wrapAndRethrow(it) } }

    private fun <R, S> wrapFlowResponseCall(implementation: (R) -> Flow<S>): (R) -> Flow<S> =
        { implementation(it).catch { e -> wrapAndRethrow(e) } }

    private fun wrapAndRethrow(e: Throwable): Nothing {
        throw when (e) {
            is AnswerContractException -> e.answerContract.toStatusRuntimeException()
            else -> e.toAnswerContract().toStatusRuntimeException()
        }
    }
}