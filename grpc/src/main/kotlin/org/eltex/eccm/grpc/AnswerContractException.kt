package org.eltex.eccm.grpc

class AnswerContractException(
    val answerContract: AnswerContract
) : Exception(answerContract.message) {

    constructor(code: Int, message: String, reason: String) : this(AnswerContract(code, message, reason))
}
