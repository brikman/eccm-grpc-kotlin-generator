package org.eltex.eccm.grpc.client

import io.grpc.NameResolverProvider
import io.grpc.NameResolverRegistry
import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy

class NameResolverRegistrar(
    private val providers: List<NameResolverProvider>
) {
    private val registry = NameResolverRegistry.getDefaultRegistry()

    @PostConstruct
    fun init() {
        providers.forEach { registry.register(it) }
    }

    @PreDestroy
    fun destroy() {
        providers.forEach { registry.deregister(it) }
    }
}