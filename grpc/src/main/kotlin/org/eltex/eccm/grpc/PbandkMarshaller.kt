package org.eltex.eccm.grpc

import io.grpc.MethodDescriptor
import pbandk.Message
import pbandk.decodeFromStream
import pbandk.encodeToByteArray
import java.io.ByteArrayInputStream
import java.io.InputStream

class PbandkMarshaller<T : Message>(
    private val loader: Message.Companion<T>
) : MethodDescriptor.Marshaller<T> {

    override fun stream(value: T): InputStream =
        ByteArrayInputStream(value.encodeToByteArray())

    override fun parse(stream: InputStream): T =
        loader.decodeFromStream(stream)
}
