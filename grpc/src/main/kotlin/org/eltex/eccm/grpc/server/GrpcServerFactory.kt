package org.eltex.eccm.grpc.server

import io.grpc.Server
import io.grpc.ServerInterceptor
import io.grpc.ServerInterceptors
import io.grpc.ServerServiceDefinition
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

class GrpcServerFactory(
    private val properties: GrpcServerProperties,
    private val serviceBeans: List<AbstractGrpcService>,
    private val globalInterceptors: List<ServerInterceptor>,
) {

    fun getServer(): Server {
        val builder = NettyServerBuilder.forPort(properties.port)

        configureServices(builder)

        // TODO AbstractGrpcServerFactory.configure()

        return builder.build()
    }

    private fun configureServices(builder: NettyServerBuilder) {
        val serviceNames = mutableSetOf<String>()
        val serviceDefinitions = buildServiceDefinitions()
        serviceDefinitions.forEach { service ->
            if (!serviceNames.add(service.serviceName)) {
                throw IllegalStateException(
                    "Multiple implementations of service '${service.serviceName}' found in context"
                )
            }
            builder.addService(service.serviceDefinition)
        }
        if (serviceDefinitions.isNotEmpty()) {
            val registeredServicesSummary = buildString {
                append("Total ${serviceDefinitions.size} gRPC services registered:")
                serviceDefinitions.forEach { service ->
                    append("\n\t- ${service.serviceName}: ${service.beanClass.qualifiedName}")
                    if (service.interceptorsChain.isNotEmpty()) {
                        service.interceptorsChain.joinTo(
                            buffer = this,
                            separator = " → ",
                            prefix = " (interceptors chain: ",
                            postfix = ")"
                        ) { "${it::class.qualifiedName}" }
                    }
                }
            }
            logger.info(registeredServicesSummary)
        }
    }

    private fun buildServiceDefinitions(): List<GrpcServiceDefinition> {
        return serviceBeans.map { serviceBean ->
            val serviceDefinition = ServerInterceptors
                .interceptForward(serviceBean.bindService(), globalInterceptors)
            GrpcServiceDefinition(
                beanClass = serviceBean::class,
                serviceDefinition = serviceDefinition,
                interceptorsChain = globalInterceptors,
            )
        }
    }

    private data class GrpcServiceDefinition(
        val beanClass: KClass<*>,
        val serviceDefinition: ServerServiceDefinition,
        val interceptorsChain: List<ServerInterceptor>,
    ) {
        val serviceName: String = serviceDefinition.serviceDescriptor.name
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.declaringClass)
    }
}
