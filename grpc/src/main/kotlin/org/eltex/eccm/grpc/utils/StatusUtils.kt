package org.eltex.eccm.grpc.utils

import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.grpc.protobuf.StatusProto
import org.eltex.eccm.grpc.AnswerContract
import pbandk.unpack

fun AnswerContract.toStatusRuntimeException(): StatusRuntimeException {
    val statusProto = com.google.rpc.Status.newBuilder()
        .setCode(Status.Code.INTERNAL.value())
        .setMessage(message)
        .addDetails(this.packToGrpcAny())
        .build()
    return StatusProto.toStatusRuntimeException(statusProto)
}

fun Throwable.toAnswerContract(): AnswerContract {
    val statusProto = StatusProto.fromThrowable(this)
    if (statusProto != null) {
        val details = statusProto.detailsList.firstOrNull()
        if (details != null) {
            return details.toPbandk().unpack(AnswerContract)
        }
        val code = Status.fromCodeValue(statusProto.code).code
        val reason = statusProto.message.takeIf { it.isNotEmpty() }
        return AnswerContract(
            code = 500,
            message = "Internal error (code: ${code.name})",
            reason = reason ?: "Unknown error"
        )
    }
    return AnswerContract(
        code = 500,
        message = "Unknown error",
        reason = "Unknown error"
    )
}
