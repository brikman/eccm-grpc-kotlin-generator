package org.eltex.eccm.grpc.client

import io.grpc.NameResolverProvider
import io.grpc.internal.RediscoveringDnsNameResolverProvider
import org.eltex.eccm.grpc.GrpcCommonAutoConfiguration
import org.eltex.eccm.grpc.context.MetadataForwarder
import org.eltex.eccm.grpc.context.MetadataForwarders
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean

@AutoConfiguration(after = [GrpcCommonAutoConfiguration::class])
@EnableConfigurationProperties(GrpcClientProperties::class)
class GrpcClientAutoConfiguration {

    @Bean
    fun grpcClient(
        grpcClientProperties: GrpcClientProperties,
        metadataForwarders: MetadataForwarders
    ): GrpcClient {
        return GrpcClient(grpcClientProperties, metadataForwarders)
    }

    @Bean
    fun refreshableDnsResolverProvider(
        grpcClientProperties: GrpcClientProperties
    ): RediscoveringDnsNameResolverProvider? {
        if (grpcClientProperties.rediscoverInterval.isZero) return null
        return RediscoveringDnsNameResolverProvider(grpcClientProperties)
    }

    @Bean
    fun nameResolverRegistrar(
        nameResolverProviders: List<NameResolverProvider>
    ): NameResolverRegistrar {
        return NameResolverRegistrar(nameResolverProviders)
    }
}
