package org.eltex.eccm.grpc.client

import org.hibernate.validator.constraints.time.DurationMin
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.validation.annotation.Validated
import java.net.URI
import java.time.Duration

@Validated
@ConfigurationProperties(prefix = "grpc.client")
data class GrpcClientProperties(

    val channels: Map<String, GrpcChannelProperties> = emptyMap(),

    @field:DurationMin(seconds = 0)
    val rediscoverInterval: Duration = Duration.ofSeconds(30),

    @field:DurationMin(millis = 0)
    val defaultTimeout: Duration = Duration.ofSeconds(5),

    val defaultRetries: Int = 0,
) {

    data class GrpcChannelProperties(
        val address: URI,
    )

    companion object {
        /**
         * @see io.grpc.util.SecretRoundRobinLoadBalancerProvider.getPolicyName
         */
        const val ROUND_ROBIN_LOAD_BALANCING_STRATEGY = "round_robin"
    }
}
