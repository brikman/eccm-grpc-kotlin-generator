package org.eltex.eccm.grpc.context

import java.util.UUID
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

class TraceId(
    val uuid: UUID
) : AbstractCoroutineContextElement(TraceId) {

    override fun toString(): String {
        return uuid.toString()
    }

    companion object Key : CoroutineContext.Key<TraceId> {
        fun generate(): TraceId = TraceId(UUID.randomUUID())
    }
}
