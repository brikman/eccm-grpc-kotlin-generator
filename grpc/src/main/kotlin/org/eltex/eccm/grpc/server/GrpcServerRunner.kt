package org.eltex.eccm.grpc.server

import io.grpc.Server
import org.slf4j.LoggerFactory
import org.springframework.context.SmartLifecycle
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread

class GrpcServerRunner(
    private val grpcServerFactory: GrpcServerFactory,
) : SmartLifecycle {

    private var server: Server? = null

    @Synchronized
    override fun start() {
        if (server == null) {
            val localServer = grpcServerFactory.getServer()
            server = localServer
            localServer.start()

            logger.info("gRPC server started on port ${localServer.port}")

            // Prevent the JVM from shutting down while the server is running
            thread(
                start = true,
                isDaemon = false,
                name = "grpc-server-container-${serverCounter.incrementAndGet()}"
            ) {
                try {
                    localServer.awaitTermination()
                } catch (e: InterruptedException) {
                    Thread.currentThread().interrupt()
                }
            }
        }
    }

    @Synchronized
    override fun stop() {
        server?.also { localServer ->
            logger.info("Shutting down gRPC server...")

            localServer.shutdown()
            try {
                localServer.awaitTermination(30, TimeUnit.SECONDS)
            } catch (e: InterruptedException) {
                Thread.currentThread().interrupt()
            } finally {
                localServer.shutdownNow()
                server = null
            }

            logger.info("gRPC server stopped")
        }
    }

    override fun isRunning(): Boolean {
        return this.server != null && this.server?.isShutdown == true
    }

    override fun isAutoStartup(): Boolean {
        return true
    }

    override fun getPhase(): Int {
        return Int.MAX_VALUE
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.declaringClass)

        private val serverCounter = AtomicInteger(0)
    }
}