package org.eltex.eccm.sender

import kotlinx.coroutines.runBlocking
import org.eltex.eccm.grpc.AnswerContractException
import org.eltex.eccm.grpc.TestServiceGrpcClient.send
import org.eltex.eccm.grpc.client.GrpcClient
import org.eltex.eccm.grpc.context.TraceId
import org.eltex.eccm.grpc.SendRequest
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class Sender(
    private val grpcClient: GrpcClient,
) {
    private var counter = 0

    @Scheduled(fixedDelay = 1000)
    fun send() {
        runBlocking(TraceId.generate()) {
            val messageNumber = ++counter
            logger.info("[${coroutineContext[TraceId]}] Send message $messageNumber")

            try {
                grpcClient.send(SendRequest(text = "Message $messageNumber"), retries = 10)
            } catch (e: AnswerContractException) {
                logger.error("Failed to send message: $e (answer contract: ${e.answerContract})")
            } catch (e: Exception) {
                logger.error("Unknown error", e)
            }
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.declaringClass)
    }
}
