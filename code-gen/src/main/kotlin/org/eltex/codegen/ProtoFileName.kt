package org.eltex.codegen

import com.google.protobuf.DescriptorProtos
import com.google.protobuf.compiler.PluginProtos

data class ProtoFileName(
    private val path: String
) : Comparable<ProtoFileName> {
    val name: String = path.substringAfterLast('/').removeSuffix(".proto")

    override operator fun compareTo(other: ProtoFileName): Int = path.compareTo(other.path)
}

val DescriptorProtos.FileDescriptorProto.fileName: ProtoFileName
    get() = ProtoFileName(name)

val DescriptorProtos.FileDescriptorProto.dependencyNames: List<ProtoFileName>
    get() = dependencyList.map(::ProtoFileName)

val PluginProtos.CodeGeneratorRequest.filesToGenerate: List<ProtoFileName>
    get() = fileToGenerateList.map(::ProtoFileName)
