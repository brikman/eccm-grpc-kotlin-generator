package org.eltex.codegen

import com.google.protobuf.DescriptorProtos
import com.google.protobuf.Descriptors
import com.google.protobuf.ExtensionRegistry
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorRequest
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorResponse
import io.grpc.MethodDescriptor
import org.apache.commons.text.StringSubstitutor
import org.eltex.eccm.grpc.ServiceOptions
import org.eltex.eccm.grpc.ServiceOptions.ServiceName
import java.io.IOException

object CodeGenerator {
    private const val CLIENT_METHODS = "client_methods"
    private const val SERVER_METHODS = "server_methods"
    private const val SERVER_METHOD_DESCRIPTORS = "server_method_descriptors"
    private const val SERVER_METHOD_BINDINGS = "server_method_bindings"
    private const val METHOD_DESCRIPTORS = "method_descriptors"

    @JvmStatic
    fun main(args: Array<String>): Unit = generate()

    private fun generate() {
        val input = System.`in`
        val output = System.out

        val extensionRegistry = ExtensionRegistry.newInstance().apply {
            add(ServiceOptions.service)
        }

        val generatorRequest = try {
            input.buffered().use {
                CodeGeneratorRequest.parseFrom(it, extensionRegistry)
            }
        } catch (e: Exception) {
            throw IOException("Failed to parse CodeGeneratorRequest", e)
        }

        output.buffered().use { buffer ->
            val responseBuilder = CodeGeneratorResponse.newBuilder()
            responseBuilder.supportedFeatures = CodeGeneratorResponse.Feature.FEATURE_PROTO3_OPTIONAL_VALUE.toLong()

            val descriptorMap = descriptorMap(generatorRequest.protoFileList)

            generatorRequest.filesToGenerate.forEach { protoFileName ->
                val fileDescriptor = descriptorMap.getValue(protoFileName)
                val servicePackage = fileDescriptor.options.javaPackage

                fileDescriptor.services.forEach { protoService ->
                    val serviceName = protoService.name
                    val serviceFullName = protoService.fullName
                    val serviceOptions = protoService.options

                    val targetServiceName = serviceOptions.getExtension(ServiceOptions.service)
                    if (targetServiceName == ServiceName.NO_SERVICE) {
                        throw Exception(
                            """
                                Service generation error!
                                Missing 'service' option. Please provide non-default 'service' option for service like so:
                                
                                import "org/eltex/eccm/service_options.proto";
                                
                                service $serviceName {
                                    option (service) = ...;
                                    ^^^^^^
                                    ...
                                }
                            """.trimIndent()
                        )
                    }

                    val targetServiceHostname = targetServiceName.valueDescriptor.options // non-null, default = empty string
                        .getExtension(ServiceOptions.hostname)
                    if (targetServiceHostname.isEmpty()) {
                        throw Exception(
                            """
                                Service generation error!
                                Missing 'hostname' option. Please provide non-empty 'hostname' option for enum value like so:

                                enum ${ServiceName::class.simpleName} {
                                    ...
                                    ${targetServiceName.name} = ${targetServiceName.valueDescriptor.index} [(hostname = "...")];
                                    ${" ".repeat(targetServiceName.name.length + targetServiceName.valueDescriptor.index.toString().length + 6)}^^^^^^^^
                                    ...
                                }
                            """.trimIndent()
                        )
                    }

                    val clientClassName = "${serviceName}GrpcClient"
                    val clientFullClassName = "${servicePackage}.${clientClassName}"
                    val clientClassFile = "${clientFullClassName.replace('.', '/')}.kt"

                    val serverClassName = "${serviceName}GrpcBase"
                    val serverFullClassName = "${servicePackage}.${serverClassName}"
                    val serverClassFile = "${serverFullClassName.replace('.', '/')}.kt"

                    val clientMethods = mutableListOf<String>()
                    val serverMethods = mutableListOf<String>()
                    val serverMethodBindings = mutableListOf<String>()
                    val serverMethodDescriptors = mutableListOf<String>()
                    val methodDescriptors = mutableListOf<String>()

                    protoService.methods.forEach { protoMethod ->
                        val methodName = protoMethod.name
                        val methodInputType = protoMethod.inputType
                        val methodOutputType = protoMethod.outputType
                        val methodRequestTypeFull = "${servicePackage}.${methodInputType.name}"
                        val methodResponseTypeFull = "${servicePackage}.${methodOutputType.name}"

                        val methodDescriptorName = "${methodName}MethodDescriptor"

                        val callType = protoMethod.callType

                        val methodArgumentName = if (callType.pluralArgument) {
                            "requests"
                        } else {
                            "request"
                        }

                        val methodArgument = if (callType.pluralArgument) {
                            "$methodArgumentName: Flow<$methodRequestTypeFull>"
                        } else {
                            "$methodArgumentName: $methodRequestTypeFull"
                        }

                        val methodReturnType = if (callType.pluralResult) {
                            "Flow<$methodResponseTypeFull>"
                        } else {
                            methodResponseTypeFull
                        }

                        val clientCallDefinition =
                            when (callType) {
                                CallType.UNARY -> "unaryRpc"
                                CallType.CLIENT_STREAMING -> "clientStreamingRpc"
                                CallType.SERVER_STREAMING -> "serverStreamingRpc"
                                CallType.BIDI_STREAMING -> "bidiStreamingRpc"
                            }

                        val serverCallDefinition =
                            when (callType) {
                                CallType.UNARY -> "unaryServerMethodDefinition"
                                CallType.CLIENT_STREAMING -> "clientStreamingServerMethodDefinition"
                                CallType.SERVER_STREAMING -> "serverStreamingServerMethodDefinition"
                                CallType.BIDI_STREAMING -> "bidiStreamingServerMethodDefinition"
                            }


                        methodDescriptors += """
                            internal val $methodDescriptorName by lazy { 
                                io.grpc.MethodDescriptor.newBuilder<$methodRequestTypeFull, $methodResponseTypeFull>()
                                    .setType(${callType.fullGrpcMethodTypeName})
                                    .setFullMethodName("${'$'}SERVICE_NAME/$methodName")
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller($methodRequestTypeFull.marshaller())
                                    .setResponseMarshaller($methodResponseTypeFull.marshaller())
                                    .build()
                            }
                        """.trimIndent()

                        serverMethodDescriptors += ".addMethod($serverClassName.$methodDescriptorName)"

                        clientMethods += """
                            /**
                             * Service method: [$serverFullClassName.$methodName]
                             */
                            ${if (callType.suspendClient) "suspend " else ""}fun org.eltex.eccm.grpc.client.GrpcClient.$methodName(
                                $methodArgument,
                                timeout: Duration? = null,
                                retries: Int? = null,
                            ): $methodReturnType =
                                ${clientCallDefinition}(${methodArgumentName}, ${serverClassName}.${methodDescriptorName}, TARGET, timeout, retries)
                        """.trimIndent()

                        serverMethods += """
                            /**
                             * Client method: [$clientFullClassName.$methodName]
                             */
                            abstract ${if (callType.suspendServer) "suspend " else ""}fun ${methodName}(
                                $methodArgument
                            ): $methodReturnType
                        """.trimIndent()

                        serverMethodBindings += ".addMethod(${serverCallDefinition}(${serverClassName}.${methodDescriptorName}, ::${methodName}))"
                    }

                    val renderedClientMethods = clientMethods.joinIndented(separateWithEmptyLine = true, indents = 1)

                    val clientClassContent = """
                        package $servicePackage
                        
                        import kotlinx.coroutines.flow.Flow
                        import org.eltex.eccm.grpc.client.GrpcTarget
                        import javax.annotation.Generated
                        import kotlin.time.Duration
                        
                        @Generated
                        object $clientClassName {
                        
                            private val TARGET = GrpcTarget(
                                serviceName = "${targetServiceName.name}", 
                                hostname = "$targetServiceHostname"
                            )
                        
                            %$CLIENT_METHODS%
                        }
                        
                    """.render(
                        CLIENT_METHODS to renderedClientMethods,
                    )

                    val renderedServerMethods =
                        serverMethods.joinIndented(separateWithEmptyLine = true, indents = 1)
                    val renderedDescriptors =
                        methodDescriptors.joinIndented(separateWithEmptyLine = false, indents = 2)
                    val renderedServerMethodDescriptors =
                        serverMethodDescriptors.joinIndented(separateWithEmptyLine = false, indents = 4)
                    val renderedServerMethodBindings =
                        serverMethodBindings.joinIndented(separateWithEmptyLine = false, indents = 3)

                    val serverClassContent = """
                        package $servicePackage
                        
                        import kotlinx.coroutines.flow.Flow
                        import org.eltex.eccm.grpc.TestServiceGrpcClient.bidiStream
                        import org.eltex.eccm.grpc.TestServiceGrpcClient.clientStream
                        import org.eltex.eccm.grpc.TestServiceGrpcClient.send
                        import org.eltex.eccm.grpc.TestServiceGrpcClient.serverStream
                        import org.eltex.eccm.grpc.context.MetadataCoroutineContext
                        import org.eltex.eccm.grpc.server.AbstractGrpcService
                        import javax.annotation.Generated
                        import kotlin.coroutines.CoroutineContext
                        import kotlin.coroutines.EmptyCoroutineContext
                        
                        @Generated
                        abstract class $serverClassName(
                            coroutineContext: CoroutineContext = EmptyCoroutineContext
                        ) : AbstractGrpcService(coroutineContext) {
                        
                            %$SERVER_METHODS%
                            
                            final override fun bindService(): io.grpc.ServerServiceDefinition {
                                return io.grpc.ServerServiceDefinition.builder(serviceDescriptor)
                                    %$SERVER_METHOD_BINDINGS%
                                    .build()
                            }
                            
                            protected suspend fun metadata(): io.grpc.Metadata =
                                MetadataCoroutineContext.getMetadata()
                            
                            companion object {
                                internal const val SERVICE_NAME = "$serviceFullName"
                                
                                internal val serviceDescriptor by lazy {
                                    io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
                                        %$SERVER_METHOD_DESCRIPTORS%
                                        .build()
                                }
                            
                                %$METHOD_DESCRIPTORS%
                                
                                private inline fun <reified T : pbandk.Message> pbandk.Message.Companion<T>.marshaller(): io.grpc.MethodDescriptor.Marshaller<T> = 
                                    org.eltex.eccm.grpc.PbandkMarshaller(this)
                            }
                        }
                        
                    """.render(
                        SERVER_METHODS to renderedServerMethods,
                        SERVER_METHOD_DESCRIPTORS to renderedServerMethodDescriptors,
                        SERVER_METHOD_BINDINGS to renderedServerMethodBindings,
                        METHOD_DESCRIPTORS to renderedDescriptors,
                    )

                    responseBuilder.addFile(
                        CodeGeneratorResponse.File.newBuilder().also { file ->
                            file.name = clientClassFile
                            file.content = clientClassContent
                        }.build()
                    )

                    responseBuilder.addFile(
                        CodeGeneratorResponse.File.newBuilder().also { file ->
                            file.name = serverClassFile
                            file.content = serverClassContent
                        }.build()
                    )
                }
            }

            responseBuilder.build().writeTo(buffer)
        }
    }

    private fun descriptorMap(
        topologicalSortedProtoFileList: List<DescriptorProtos.FileDescriptorProto>
    ): Map<ProtoFileName, Descriptors.FileDescriptor> {
        val descriptorsByName = mutableMapOf<ProtoFileName, Descriptors.FileDescriptor>()
        for (protoFile in topologicalSortedProtoFileList) {
            val dependencies = protoFile.dependencyNames.map(descriptorsByName::getValue)
            val fileDescriptor = Descriptors.FileDescriptor.buildFrom(protoFile, dependencies.toTypedArray())
            descriptorsByName[protoFile.fileName] = fileDescriptor
        }
        return descriptorsByName
    }

    private fun List<String>.joinIndented(
        separateWithEmptyLine: Boolean,
        indents: Int
    ): String {
        val indentation = "\t".repeat(indents)
        return buildString {
            this@joinIndented.forEachIndexed { lineNumber, line ->
                if (lineNumber == 0) {
                    val subLines = line.lines()
                    if (subLines.size == 1) {
                        append(line)
                    } else {
                        subLines.forEachIndexed subLines@{ subLineNumber, subLine ->
                            if (subLineNumber == 0) {
                                appendLine(subLine)
                            } else {
                                append(subLine.prependIndent(indentation))
                                if (subLineNumber < subLines.lastIndex) {
                                    appendLine()
                                }
                            }
                        }
                    }
                } else {
                    append(line.prependIndent(indentation))
                }
                if (lineNumber < this@joinIndented.lastIndex) {
                    appendLine()
                    if (separateWithEmptyLine) {
                        appendLine()
                    }
                }
            }
        }
    }

    private fun String.render(vararg parameters: Pair<String, String>): String =
        trimIndent().run { StringSubstitutor.replace(this, parameters.toMap(), "%", "%") }

    private enum class CallType(
        val pluralArgument: Boolean,
        val pluralResult: Boolean,
        val suspendClient: Boolean,
        val suspendServer: Boolean,
        val grpcMethodType: MethodDescriptor.MethodType,
    ) {
        UNARY(
            pluralArgument = false,
            pluralResult = false,
            suspendClient = true,
            suspendServer = true,
            grpcMethodType = MethodDescriptor.MethodType.UNARY
        ),
        CLIENT_STREAMING(
            pluralArgument = true,
            pluralResult = false,
            suspendClient = true,
            suspendServer = true,
            grpcMethodType = MethodDescriptor.MethodType.CLIENT_STREAMING
        ),
        SERVER_STREAMING(
            pluralArgument = false,
            pluralResult = true,
            suspendClient = true,
            suspendServer = false,
            grpcMethodType = MethodDescriptor.MethodType.SERVER_STREAMING
        ),
        BIDI_STREAMING(
            pluralArgument = true,
            pluralResult = true,
            suspendClient = true,
            suspendServer = false,
            grpcMethodType = MethodDescriptor.MethodType.BIDI_STREAMING
        );

        val fullGrpcMethodTypeName: String = "${grpcMethodType::class.qualifiedName}.$name"
    }

    private val Descriptors.MethodDescriptor.callType: CallType
        get() = when {
            isServerStreaming && isClientStreaming -> CallType.BIDI_STREAMING
            isClientStreaming -> CallType.CLIENT_STREAMING
            isServerStreaming -> CallType.SERVER_STREAMING
            else -> CallType.UNARY
        }
}
